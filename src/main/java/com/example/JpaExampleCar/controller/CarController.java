package com.example.JpaExampleCar.controller;

import com.example.JpaExampleCar.model.CarModel;
import com.example.JpaExampleCar.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {
    @Autowired
    CarService carServiceObj;
    @PostMapping("/addcardetails")
    public String addCarDetails(@RequestBody CarModel carModelObj){
        String str=carServiceObj.addCarDetails(carModelObj);
        return str;
    }

    @GetMapping("/cardetailsbyid/{id}")
    public List<CarModel> fetchCarDetailsByID(@PathVariable int id){
        return carServiceObj.getfetchCarDetailsByID(id);
    }
    @GetMapping("/cardetailsbyname/{carName}")
    public List<CarModel> fetchCarDetailsByName(@PathVariable String carName){
        return carServiceObj.getfetchCarDetailsByName(carName);
    }

    @DeleteMapping("/deletecardetailsbyid/{id}")
    public void deleteCarDetailsById(@PathVariable int id){
        carServiceObj.deletecarDetails(id);
    }
}
