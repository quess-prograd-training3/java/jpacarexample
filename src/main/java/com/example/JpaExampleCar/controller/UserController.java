package com.example.JpaExampleCar.controller;

import com.example.JpaExampleCar.model.User;
import com.example.JpaExampleCar.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class UserController {
    @Autowired
    UserService userServiceObj;
    @PostMapping("/adding")
    public String addUser(@RequestBody User userObj){
        String str=String.valueOf(userServiceObj.addUsers(userObj));
        return str;
    }
    @GetMapping("/getUserById/{userId}")
    public Optional<User> display(@PathVariable int userId){
        return userServiceObj.getUser(userId);
    }
}
