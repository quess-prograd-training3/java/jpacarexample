package com.example.JpaExampleCar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaExampleCarApplication {

	public static void main(String[] args) {

		SpringApplication.run(JpaExampleCarApplication.class, args);
	}

}
