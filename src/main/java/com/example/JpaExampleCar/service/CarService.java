package com.example.JpaExampleCar.service;

import com.example.JpaExampleCar.model.CarModel;
import com.example.JpaExampleCar.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CarService {
    @Autowired
    CarRepository carRepositoryObj;

    public String addCarDetails(CarModel carModelObj) {
        carRepositoryObj.save(carModelObj);
        return "Car Details Added";
    }

    public List<CarModel> getfetchCarDetailsByID(int id) {
        return carRepositoryObj.findByid(id);
    }

    public List<CarModel> getfetchCarDetailsByName(String carName) {
        return carRepositoryObj.findBycarName(carName);
    }

    public void deletecarDetails(int id) {
        carRepositoryObj.deleteById(id);
    }
}
