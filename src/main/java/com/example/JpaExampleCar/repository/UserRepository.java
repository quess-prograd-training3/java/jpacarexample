package com.example.JpaExampleCar.repository;

import com.example.JpaExampleCar.model.User;

import java.util.List;

public interface UserRepository {
    List<User> findAll();
}
