package com.example.JpaExampleCar.repository;

import com.example.JpaExampleCar.model.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<CarModel, Integer> {
    List<CarModel> findBycarName(String carName);
    List<CarModel> findByid(int id);
}
