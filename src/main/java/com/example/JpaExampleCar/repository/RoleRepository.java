package com.example.JpaExampleCar.repository;

import java.util.Optional;

public interface RoleRepository {
    Optional<Object> findById(int i);
}
