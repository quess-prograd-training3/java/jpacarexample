package com.example.JpaExampleCar.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class CarModel {
    @Id
    private int id;
    private String carName;
    private int price;

    public CarModel() {
    }

    public CarModel(int id, String carName, int price) {
        this.id = id;
        this.carName = carName;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
